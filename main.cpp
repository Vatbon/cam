#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <time.h>
using namespace cv;
int main(int argc,char *argv[])
{
CvCapture *capture = cvCreateCameraCapture(CV_CAP_ANY);
assert(capture);

double width = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_WIDTH);
double height = cvGetCaptureProperty(capture, CV_CAP_PROP_FRAME_HEIGHT);
printf("[i] %.0f x %.0f\n", width, height );
double diff, sumfps, sumtime;
IplImage* frame=0;

cvNamedWindow("capture", CV_WINDOW_AUTOSIZE);
uchar bw;
struct timespec start, end;
sumfps = 0;
sumtime = 0 ;
while(1) {
	clock_gettime(CLOCK_MONOTONIC_RAW, &start);
	frame = cvQueryFrame(capture);
	assert(frame);
	for (int y=0; y < frame->height; y++){
		uchar *ptr = (uchar*)(frame->imageData + y*frame->widthStep);
		for (int x = 0; x < frame->width; x++){
			if ((x-width/2)*(x-width/2) + (y-height/2)*(y-height/2) <= height*height/8){
				ptr[3*x] = 0;
				ptr[3*x + 1] = 0;
			}
			else{
				bw = (ptr[3*x] + ptr[3*x + 1] + ptr[3*x + 2])/3;
				ptr[3*x] = bw * 1.5 > 255 ? 255 : bw * 1.5; // Blue
				ptr[3*x + 1] = bw * 1.5 > 255 ? 255 : bw * 1.5; //Green 0=Blue 1=Green 2=Red
				ptr[3*x + 2] = bw * 1.5 > 255 ? 255 : bw * 1.5; //Red
			}
		}
	}
	cvShowImage("capture", frame);
	clock_gettime(CLOCK_MONOTONIC_RAW, &end);
	diff = (end.tv_sec - start.tv_sec) + 0.000000001*(end.tv_nsec - start.tv_nsec);
        printf("Time taken: %lf sec.\nFPS: %lf\n", diff, 1/diff);
	sumfps++; sumtime+= diff;
	char c = cvWaitKey(5);
	if(c == 27)
		 break;
	}
printf("Average FPS: %lf\nWhole time: %lf sec.\nAverage time: %lf sec.\n", sumfps/sumtime, sumtime, sumtime/sumfps);
cvReleaseCapture(&capture);
cvDestroyWindow("capture");
return 0;
}
